(function ($) {
    var parent = jc.Plugin;
    var prototype = jc.makePluginPrototype(
        'demo.PluginA',
        'demo_pluginA',
        parent,
        {
            message: 'Hello world!'
        }
    );
    
    
    prototype.init = function () {
        var self = this;
        
        self.elButton = self.find('button');
        self.elButton.click(function () {
            self.showMessage();
        });
        
        parent.prototype.init.call(self);
    };
    
    prototype.showMessage = function () {
        var self = this;
        alert(self.cfg.message);
        return self.el();
    };
})(jQuery);