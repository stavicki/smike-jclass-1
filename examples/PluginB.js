(function ($) {
    var parent = demo.PluginA;
    var prototype = jc.makePluginPrototype(
        'demo.PluginB',
        'demo_pluginB',
        parent
    );
    
    prototype.showMessage = function () {
        var self = this;
        parent.prototype.showMessage.call(this);
        confirm("Are you sure!");
        return self.el();
    };
})(jQuery);